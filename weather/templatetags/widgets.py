from django import template 

register = template.Library()


@register.inclusion_tag('weather/weather_widget.html')
def weather_widget(n):
    cities = {
            'city': 'Zielona Gora',
            }
    return {'cities': cities}

