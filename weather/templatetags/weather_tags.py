from django import template
from datetime import datetime

register = template.Library()


@register.simple_tag
def create_date(date):
    string_date = date.strftime('%Y-%m-%d')
    message = f'Data utworzenia {string_date}'
    return message

@register.simple_tag
def add_number(n1, n2):
    print("####, ", type(n1))
    return n1 + n2
