from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render

import datetime

def main(request):
    temp = get_temp()

    context = {
        'name': 442,
        'temperature': temp,
        'is_user': is_user(),
        'calc': calculate(),
        'today': datetime.date.today()
    }
    return render(request, "weather/main.html", context)


def get_temp():
    return 2.0

def example(request, name):
    print(name, type(name))
    name += "LOL"
    return render(request, "weather/other.html", {'name': name})

def is_user():
    return True

def calculate():
    return {'item1': 1, 'item2': 2, 'item3': 3, 'item4': 4}
