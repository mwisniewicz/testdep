from django.shortcuts import render

from converters import forms

def convert(request):
    temp = 0
    form = forms.ConverterForm()

    if request.method == 'POST':
        form = forms.ConverterForm(request.POST)
        if form.is_valid():
            temp = request.POST['temperature']
        else:
            print(form.errors)
    else:
        form = forms.ConverterForm()

    context = {'form': form, 'temp': temp}
    return render(request, "converters/main.html", context)