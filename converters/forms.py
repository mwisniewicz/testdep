from django import forms
from django.core.exceptions import ValidationError

class ConverterForm(forms.Form):
    temperature = forms.FloatField(label='Temperatura', max_value=100.0, min_value=1.0)

    def clean_temperature(self):
        #print("CCC")
        #print(self.cleaned_data)
        temperature = self.cleaned_data['temperature']
        if temperature:
            temp = float(temperature)
            if temp < 10.0:
                raise ValidationError("Temperatura musi byc większa niż 10.0")